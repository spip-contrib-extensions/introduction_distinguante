<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-introduction_distinguante
// Langue: fr
// Date: 21-02-2022 14:03:48
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'introduction_distinguante_description' => 'Fournit une balise <code>#INTRODUCTION_DISTINGUANTE</code> qui fonctionne comme <code>#INTRODUCTION</code> mais entoure la partie provenante du chapo par un <code>span.introduction-chapo</code> et celle provenant du texte par un <code>span.introduction-texte</code>.',
	'introduction_distinguante_slogan' => 'Ne mélangez pas le chapeau et le texte.',
);
?>
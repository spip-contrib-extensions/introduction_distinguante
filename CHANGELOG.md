# Changelog

## 2.0.0 - 2023-06-10

### Added

- Compatiblité SPIP 4.2

### Removed

- Compatible SPIP 3.2
